const express = require("express");
// Mongoose is a package that allows creation of schemas to model our data structures and to have an access to a number of methods for manipulating our database

const mongoose = require("mongoose");
const app = express();
const port = 4000;

// MongoDB Connection
// connectiong to MongoDB Atlas

mongoose.connect("mongodb+srv://dbUser:dbUser@wdc028-course-booking.0pav9.mongodb.net/batch144-to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology:true
})

// Set notification for connection success or failure
let db = mongoose.connection;
// if error occured, output in the console
db.on("error", console.error.bind(console, "connection error"))
// if the connection is succesful, output in the console
db.once("open", () => console.log("We're connected to the cloud database"))


app.use(express.json())
app.use(express.urlencoded({extended:true}))



// Mongoose Schemas

// Schemas determine the structure of the documents to be written in the database, it acts as a blueprint to our data
// We will use Schema() constructor of the Mongoose module to create a new Schema object

const taskSchema = new mongoose.Schema({
	name: String,
	status:{
		type: String,
		// Default values are the predefined values for a field if we dont put any value
		default: "pending"
	}
})

const Task = mongoose.model("Task", taskSchema)
// Models are what allows us to gain access to methods that will perform CRUD functions. 
// Models must be in singular form and capitalized following the MVC approach for naming conventions
// firstparameter of the mongoose model methof indicates the collection in where to store the data
// second parameter is used to specify the Schema/Blueprint



// Routes

// Create a new task
 //Business Logic
// 1. Add a functionality to check if there are duplicate tasks
// if the task already exists, return there is a duplicate 
// if the task doesnt exist, we can add it in the database
// 2. The task data will be coming from the request's body
// 3. Create new Task object with properties that we need
// 4. Then save the data



app.post("/tasks", (req,res) => {
	// Check if there are duplicate tasks
	// findOne() is a mongoose method that acts similar to "find"
	// it returns the first document that matches the search criteria
	Task.findOne({name: req.body.name}, (error, result) => {
		// if a document was found and rhe document's name matches the information sent via client/postman
		if(result !== null && result.name == req.body.name){
			// return a message to the client/postman
			return res.send("Duplicate task found")
		}else{
			// if no document was found
			// create a new task and save it to the database
		let newTask = new Task({
			name: req.body.name

			})

			newTask.save((saveError, savedTask) => {
				// if there are errors in saving
				if(saveError){
					// will print any errors found in the console
					// saveERr is an error object that will contain details about that erro
					// Erros normally come as an object data type
					return console.error(saveError)
			}else{
				// no error found while creating the document
				// return a status code of 201 for sucessful creation
				return res.status(201).send("New Task Created")
			}
		})

		}
	})
})

// Retrieving all data
// Business Logic
// 1. Retrieve all the documents using the find()
// 2. if an error is encountered, print the error
// 3. if no errors are found, send a success status back to the client and return an array of documents

app.get("/tasks", (req,res) => {
	// an empty "{}" means it returns all the documents and stores them in the "result" parameter of the call back function
	Task.find( {},(err,result) => {
		if(err){
			return console.log(err)
		} else{
			return res.status(200).json({
				data: result
			})
		}
	} )
})

// Register a user(Business Logic)
// 1.) Find if there are duplicate user
	// if user already exists, we return an error
	// if user doesnt exist, we add it in the database
	// if the username and password are both not blank, 
		// if blank, send response "both username and password must be provided"
		// both condition has been met, create a new project
		// save the new object
			// if error, return an error
			// else, response a status 201 and "new user registered"

const userSchema = new mongoose.Schema({
	username: String,
	password: String
	})

const User = mongoose.model("User", userSchema)

app.post("/signup", (req,res) => {
	User.findOne({username: req.body.username},{password: req.body.password}, (error, result) => {
		
		if(result !== null && result.username == req.body.username){
			
			return res.send("User already exists")
		
		}else if (req.body.username == " " && req.body.password == " "){

		return res.send("BOTH username and password must be provided")

		}else{
			// if no user was found
			// create a new user and save it to the database
		let newUser = new User({
			username: req.body.username,
			password: req.body.password
			})

			newUser.save((saveError, savedTask) => {
				
				if(saveError){
					
					return console.error(saveError)
			}else{
			
				return res.status(201).send("New User registered")
			}
		})

		}
	})
})

app.get("/users", (req,res) => {
	User.find( {},(err,result) => {
		if(err){
			return console.log(err)
		} else{
			return res.status(200).json({
				data: result
			})
		}
	} )
})

app.listen(port, () => console.log(`Server is running at port ${port}`))